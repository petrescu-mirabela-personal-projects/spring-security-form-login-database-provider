select * from user;
select * from user_authority;
select * from authority;
select * from profile;

-- INSERT INTO `authority`(`name`, `id`) VALUES ('ROLE_ADMIN', 1);
-- INSERT INTO `authority`(`name`, `id`) VALUES ('ROLE_USER', 2);

INSERT INTO `profile`(`id`, `address`, `date_of_birth`, `first_name`, `gender`, `last_name`, `occupation`) VALUES (1,"Iowa,USA",'1993-11-15 22:14:54',"Yogen","M","Rai","Engineer");
INSERT INTO `user` (`id`, `username`, `password`, `email`,`date_created`, `profile_id`) VALUES (1,'ironman','$2a$10$jXlure/BaO7K9WSQ8AMiOu3Ih3Am3kmmnVkWWHZEcQryZ8QPO3FgC','ironman@gmail.com','2015-11-15 22:14:54',1);

INSERT INTO `profile`(`id`, `address`, `date_of_birth`, `first_name`, `gender`, `last_name`, `occupation`) VALUES (2,"Kathmandu,Nepal",'1992-11-15 22:14:54',"Rabi","M","Maharjan","Student");
INSERT INTO `user` (`id`, `username`, `password`, `email`,`date_created`, `profile_id`) VALUES (2,'rabi','$2a$10$0tFJKcOV/Io6I3vWs9/Tju8OySoyMTpGAyO0zaAOCswMbpfma0BSK','rabi@gmail.com','2015-10-15 22:14:54',2);

INSERT INTO `profile`(`id`, `address`, `date_of_birth`, `first_name`, `gender`, `last_name`, `occupation`) VALUES (3,"Indonesia",'1992-11-15 22:14:54',"John","M","Rockie","Teacher");
INSERT INTO `user` (`id`, `username`, `password`, `email`,`date_created`, `profile_id`) VALUES (3,'rockie','$2a$10$WILylGNCM9SxeOKL2wKT9ukUlt3XEjeo1ULn/qB66duZokcOGT.G.','rockie92@gmail.com','2015-10-16 22:14:54',3);

INSERT INTO `profile`(`id`, `address`, `date_of_birth`, `first_name`, `gender`, `last_name`, `occupation`) VALUES (4,"Mumbai, India",'1990-11-15 22:14:54',"Rita","F","Khaling","Engineer");
INSERT INTO `user` (`id`, `username`, `password`, `email`,`date_created`, `profile_id`) VALUES (4,'rita','$2a$10$ANuESyLFe1PgzWQkq1Y1IeoWWYEtDa/47bmmig1di6CVZOwe7MGh6','khaling.rita.92@gmail.com','2015-10-12 22:14:54',4);

INSERT INTO `profile`(`id`, `address`, `date_of_birth`, `first_name`, `gender`, `last_name`, `occupation`) VALUES (5,"NULL",'1992-11-15 22:14:54',"NULL","M","NULL","NULL");
INSERT INTO `user` (`id`, `username`, `password`, `email`,`date_created`,`profile_id`) VALUES (5,'manish','$2a$10$jr7iN4WedV/gW7FD.jov.OrpfhBpxsPMiss3R4ZmGjNqLJgR.2ZRC','manish@gmail.com','2015-10-13 22:14:54',5);

INSERT INTO `profile`(`id`, `address`, `date_of_birth`, `first_name`, `gender`, `last_name`, `occupation`) VALUES (6,"Romania",'2019-11-15 22:14:54',"Mirabela","F","Petrescu","Programmer");
INSERT INTO `user` (`id`, `username`, `password`, `email`,`date_created`,`profile_id`) VALUES (6,'mira','$2a$16$qu/FmRyio86CSWh3XOBsCuWdER/iiBABQCAwTXI2Z4KytiGL/9qYa','manish@gmail.com','2015-10-13 22:14:54',6);

INSERT INTO `user_authority`(`authority_id`, `user_id`) VALUES (1, 1);
INSERT INTO `user_authority`(`authority_id`, `user_id`) VALUES (2, 2);
INSERT INTO `user_authority`(`authority_id`, `user_id`) VALUES (2, 3);
INSERT INTO `user_authority`(`authority_id`, `user_id`) VALUES (1, 4);
INSERT INTO `user_authority`(`authority_id`, `user_id`) VALUES (2, 5);
INSERT INTO `user_authority`(`authority_id`, `user_id`) VALUES (1, 6);

update user set password = "A$2a$10$ANuESyLFe1PgzWQkq1Y1IeoWWYEtDa/47bmmig1di6CVZOwe7MGh6" where id = 4;
update user set password = "$2a$16$q.ndjIrvGAKvjfwyQErmEejP97aovsG/dsnu2qFVdD2Mf7RoSLeqy" where id = 6;