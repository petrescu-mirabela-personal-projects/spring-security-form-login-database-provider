package com.spring.boot.security.form.login.services.impl;

import com.spring.boot.security.form.login.domain.User;
import com.spring.boot.security.form.login.exceptions.ApiException;
import com.spring.boot.security.form.login.repositories.UserRepository;
import com.spring.boot.security.form.login.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User addUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User getUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public User getUserById(int id) {
        return userRepository.findById(id).orElseThrow(IllegalArgumentException::new);
    }
}
