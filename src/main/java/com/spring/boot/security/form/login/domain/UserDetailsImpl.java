package com.spring.boot.security.form.login.domain;

import com.spring.boot.security.form.login.controllers.LoginController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.stream.Collectors;

public class UserDetailsImpl implements UserDetails {

    private static final Logger log = LogManager.getLogger(LoginController.class);

    private User user;

    public UserDetailsImpl(User user) {
        log.info("UserDetailsImpl() user : {}", user.getUsername());
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        log.info("getAuthorities() user authority : {}", user.getAuthorities());

        // extract list of roles
        return user.getAuthorities().stream()
                .map(authority -> new SimpleGrantedAuthority(String.valueOf(authority.getName())))
                .collect(Collectors.toList());
    }

    public int getId() {
        return user.getId();
    }

    @Override
    public String getPassword() {
        return user.getPassoword();
    }

    @Override
    public String getUsername() {
        return user.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public User getUserDetails() {
        return user;
    }
}
