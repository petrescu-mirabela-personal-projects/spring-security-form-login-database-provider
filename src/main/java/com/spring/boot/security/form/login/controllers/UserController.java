package com.spring.boot.security.form.login.controllers;

import com.spring.boot.security.form.login.domain.Authority;
import com.spring.boot.security.form.login.domain.Profile;
import com.spring.boot.security.form.login.domain.User;
import com.spring.boot.security.form.login.domain.types.AuthorityType;
import com.spring.boot.security.form.login.repositories.AuthorityRepository;
import com.spring.boot.security.form.login.services.ProfileService;
import com.spring.boot.security.form.login.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collections;
import java.util.Date;
import java.util.Set;

@Controller
@RequestMapping("/users")
public class UserController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private ProfileService profileService;

    @Autowired
    private AuthorityRepository authorityRepository;

    @GetMapping(value = {"/list", ""})
    public String users(Model model) {
        model.addAttribute("userList", userService.getAllUsers());
        return "allUsers";
    }

    @GetMapping(value = "/signupForm")
    public String addUser(@ModelAttribute("user") User user, Model model) {
        model.addAttribute("url", "signup");
        return "addUser";
    }

    @PostMapping(value = "/signup")
    public String addUser(@Valid @ModelAttribute("user") User user, BindingResult result) {
        if (result.hasErrors()) {
            return "addUser";
        }

        User dbUser = this.userService.getUserByUsername(user.getUsername());

        if (dbUser != null) return "redirect:/signupForm";

        user.setProfile(new Profile());
        user.setDateCreated(new Date());

        // set user authorities
        Set<Authority> authorities = Collections.singleton(this.authorityRepository.findByName(AuthorityType.ROLE_USER));
        user.setAuthorities(authorities);

        userService.addUser(user);
        return "redirect:/login";
    }

    @GetMapping(value = "/addAdminForm")
    public String addAdmin(@ModelAttribute("user") User user, Model model) {
        model.addAttribute("admin", "admin");
        model.addAttribute("url", "addAdmin");
        return "addUser";
    }

    @PostMapping(value = "/addAdmin")
    public String addAdmin(@ModelAttribute("user") User user) {
        user.setProfile(new Profile());
        user.setDateCreated(new Date());

        // set admin authorities
        Set<Authority> authorities = Collections.singleton(this.authorityRepository.findByName(AuthorityType.ROLE_ADMIN));
        user.setAuthorities(authorities);

        userService.addUser(user);
        return "redirect:/users/adminList";
    }
}
