package com.spring.boot.security.form.login.services.impl;

import com.spring.boot.security.form.login.domain.Profile;
import com.spring.boot.security.form.login.repositories.ProfileRepository;
import com.spring.boot.security.form.login.services.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProfileServiceImpl implements ProfileService {

    @Autowired
    private ProfileRepository profileRepository;

    @Override
    public Profile updateProfile(Profile profile) {
        return profileRepository.save(profile);
    }

    @Override
    public Profile getProfile(int profileId) {
        return profileRepository.findById(profileId).orElseThrow(IllegalArgumentException::new);
    }
}
