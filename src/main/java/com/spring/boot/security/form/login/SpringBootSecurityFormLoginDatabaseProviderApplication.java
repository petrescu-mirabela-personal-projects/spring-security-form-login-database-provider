package com.spring.boot.security.form.login;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@ComponentScan(basePackages = {"com.spring.boot.security.form.login"})
@EnableJpaRepositories(basePackages = "com.spring.boot.security.form.login")
@EntityScan("com.spring.boot.security.form.login.domain")
@SpringBootApplication
@EnableTransactionManagement
public class SpringBootSecurityFormLoginDatabaseProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootSecurityFormLoginDatabaseProviderApplication.class, args);

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(10); // Strength set as 16
        String encodedPassword = encoder.encode("bela");
        System.out.println("BCryptPasswordEncoder");
        System.out.println(encodedPassword);
        System.out.println("\n");
    }

}
