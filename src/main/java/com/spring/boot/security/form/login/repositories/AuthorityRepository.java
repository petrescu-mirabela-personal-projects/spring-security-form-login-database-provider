package com.spring.boot.security.form.login.repositories;

import com.spring.boot.security.form.login.domain.Authority;
import com.spring.boot.security.form.login.domain.types.AuthorityType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorityRepository extends JpaRepository<Authority, Integer> {

    Authority findByName(AuthorityType type);
}
