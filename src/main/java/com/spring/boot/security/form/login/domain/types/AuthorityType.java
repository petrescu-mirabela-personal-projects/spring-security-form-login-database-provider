package com.spring.boot.security.form.login.domain.types;

public enum AuthorityType {
    ROLE_ADMIN,
    ROLE_USER
}
