package com.spring.boot.security.form.login.services;

import com.spring.boot.security.form.login.domain.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {

    User addUser(User user);

    List<User> getAllUsers();

    User getUserByUsername(String username);

    User getUserById(int id);

}
