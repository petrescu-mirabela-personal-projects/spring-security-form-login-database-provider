package com.spring.boot.security.form.login.services;

import com.spring.boot.security.form.login.domain.Profile;
import org.springframework.stereotype.Service;

@Service
public interface ProfileService {

    Profile updateProfile(Profile profile);

    Profile getProfile(int profileId);
}
