package com.spring.boot.security.form.login.config;

import com.spring.boot.security.form.login.controllers.LoginController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity(debug = true)
@ComponentScan(basePackages = "com.spring.boot.security.form.login.services")
@ComponentScan(basePackages = "com.spring.boot.security.form.login.config")
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private static final Logger log = LogManager.getLogger(LoginController.class);

    @Autowired
    UserDetailsService userDetailsService;

    @Autowired
    private PasswordEncoder passwordEncoder;


//    @Bean
//    public BCryptPasswordEncoder passwordEncoder() {
//        log.info("passwordEncoder() method");
//        return new BCryptPasswordEncoder(31);
//    }

    // BCryptPasswordEncoder - is a password encoder
    @Bean
    PasswordEncoder passwordEncoder() {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder(10);

        log.info("passwordEncoder() ==> " + passwordEncoder);

        return passwordEncoder;
    }

    // Authentication Manager - is DaoAuthenticationProvider,
    // backed by UserDetailsService which is accessing a database via the UserRepository repository.
    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authenticationProvider());
    }

    /*
    authorizeRequests() is how we allow anonymous access to /login,/resource/** and
        secure the rest of the resource paths.

    formLogin() is used to define the login form with username and password input.
        This has other methods that we can use to configure the behavior of the form login:
        --> loginPage() – the custom login page url.
        --> loginProcessingUrl() – the URL to which we submit the username and password.
        --> defaultSuccessUrl() – the landing page after a successful login.
        --> failureUrl() – the landing page after an unsuccessful login.
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests().antMatchers("/questionTypes/admin", "/users/adminList", "/users/addAdminForm").hasRole("ADMIN")
                .and()
                .authorizeRequests().antMatchers("/wallPage", "/questions/add", "/questionTypes", "/profile/*", "/answers/**").hasAnyRole("ADMIN", "USER")
                .and()
                .authorizeRequests().antMatchers("/login", "/resources/**").permitAll()
                .and()
                .formLogin().loginPage("/login").usernameParameter("username").passwordParameter("password").permitAll()
                .loginProcessingUrl("/doLogin")
                .successForwardUrl("/postLogin")
                .failureUrl("/loginFailed")
                .permitAll()
                .and()
                .logout().logoutUrl("/doLogout").logoutSuccessUrl("/logout").permitAll();
//                .and()
//                .csrf().disable();
    }
}
