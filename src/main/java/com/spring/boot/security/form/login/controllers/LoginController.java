package com.spring.boot.security.form.login.controllers;

import com.spring.boot.security.form.login.domain.User;
import com.spring.boot.security.form.login.domain.UserDetailsImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import javax.servlet.http.HttpSession;


@SessionAttributes({"currentUser", "currentUserId"})
@Controller
public class LoginController {

    private static final Logger log = LogManager.getLogger(LoginController.class);

//    @Autowired
//    @Qualifier("springSecurityFilterChain")
//    private Filter springSecurityFilterChain;
//
//    @GetMapping(value = "/filters")
//    @ResponseBody
//    public void getFilters() {
//        FilterChainProxy filterChainProxy = (FilterChainProxy) springSecurityFilterChain;
//        List<SecurityFilterChain> list = filterChainProxy.getFilterChains();
//        list.stream()
//                .flatMap(chain -> chain.getFilters().stream())
//                .forEach(filter -> System.out.println(filter.getClass()));
//    }

    @GetMapping(value = "/login")
    public String login() {
        log.info("login() method");
        return "login";
    }

    @GetMapping(value = "/loginFailed")
    public String loginError(Model model) {
        log.info("Login attempt failed");
        model.addAttribute("error", "true");
        return "login";
    }

    @RequestMapping("/error-forbidden")
    public String errorForbidden() {
        return "error-forbidden";
    }

    @GetMapping(value = "logout")
    public String logout(SessionStatus sessionStatus) {
        SecurityContextHolder.getContext().setAuthentication(null);
        sessionStatus.setComplete();
        return "redirect:/welcome";
    }

    @PostMapping(value = "/postLogin")
    public String postLogin(Model model, HttpSession session) {
        log.info("/postLogin ==> postLogin()");

        // read principal out of security context and set it to session
        UsernamePasswordAuthenticationToken authentication =
                (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        validatePrinciple(authentication.getPrincipal());
        User loggedInUser = ((UserDetailsImpl) authentication.getPrincipal()).getUserDetails();

        model.addAttribute("currentUser", loggedInUser.getUsername());
        model.addAttribute("currentUserId", loggedInUser.getId());
        session.setAttribute("userId", loggedInUser.getId());

        return "welcome";
    }

    private void validatePrinciple(Object principal) {
        if (!(principal instanceof UserDetailsImpl)) {
            throw new IllegalArgumentException("Principal can not be null!");
        }
    }
}
